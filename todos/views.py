from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import ListForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list}
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_list_detail": todo_list_detail}
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            listcreate = form.save()
            return redirect("todo_list_detail", id=listcreate.id)
    else:
        form = ListForm()

    context = {"todo_list_create_form": form}
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = ListForm(instance=todo_list)

    context = {
        "todo_list": todo_list,
        "update_form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    list_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        list_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")
